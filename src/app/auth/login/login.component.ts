import { Component, OnInit } from '@angular/core';
import {ApiService} from "../../shared/api.service";
import {Location} from "@angular/common";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private apiService: ApiService, private router: Router) { }

  username: string;
  password: string;

  badPassword: boolean = false;

  ngOnInit(): void {
  }

  login() {
    this.apiService.login(this.username, this.password).subscribe(response => {
      localStorage.setItem('token', response.access_token);
      this.router.navigate(['/panel']).then();
      console.log(response);
    }, (err) => {
      this.badPassword = true;
    })
  }
}
