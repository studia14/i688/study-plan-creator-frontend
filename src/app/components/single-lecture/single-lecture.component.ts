import {Component, Input, OnInit} from '@angular/core';
import { Lecture } from '@study/study-plan-creator/lib/lecture';

@Component({
  selector: 'app-single-lecture',
  templateUrl: './single-lecture.component.html',
  styleUrls: ['./single-lecture.component.css']
})
export class SingleLectureComponent implements OnInit {
  @Input() lecture: Lecture | undefined;
  constructor() { }

  ngOnInit(): void {

  }

  checkLectureType = (): string  => {
    // switch (this.lecture?.lectureType) {
    //   case LectureTypes.LECTURE: {
    //     return '#9ebae8';
    //   }
    //   case LectureTypes.LABORATORY: {
    //     return  '#9ee8b2';
    //   }
    //   case LectureTypes.SEMINAR: {
    //     return '#e8df9e';
    //   }
    //   case LectureTypes.EXERCISES: {
    //     return '#e8a49e';
    //   }
    // }
    return 'white';
  };
}
