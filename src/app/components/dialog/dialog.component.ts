import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {Classroom} from "../../models/responses/classroom";

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: Classroom) {}
  // @ts-ignore
  room: Classroom = this.data.room;
  edit: string = 'edit';
  ngOnInit(): void {
    console.log(this.room);
  }

}
