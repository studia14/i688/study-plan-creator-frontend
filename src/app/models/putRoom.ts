export interface PutRoom {
  name: string;
  purposes: Array<string>
}
