import {Roles} from "./enums/roles";

export interface User {
  access_token: string;
}
