import {LectureType} from "./lectureTypes";

export interface PostSemester {
  name: string;
  degree: string;
  number: number;
  types: Array<LectureType>
}
