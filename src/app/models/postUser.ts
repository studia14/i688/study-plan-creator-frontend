export interface PostUser {
  client_id: string;
  username: string;
  password: string;
  grant_type: string;
}
