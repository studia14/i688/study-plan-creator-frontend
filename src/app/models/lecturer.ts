import { lecturerClass } from "./lecturerClass";

export interface Lecturer {
  id: number;
  name: string;
  surname: string;
  title: string;
  canTeach: Array<lecturerClass>
}

export interface PostLecturer {
  name: string;
  surname: string;
  title: string;
  canTeach: Array<lecturerClass>
}
