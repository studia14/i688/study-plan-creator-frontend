export enum LectureTypes {
  EXERCISES= 'ćwiczenia',
  LABORATORY = "laboratorium",
  SEMINAR = 'seminarium',
  LECTURE = 'wykład'
}
