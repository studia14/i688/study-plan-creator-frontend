import {RoomTypes} from "../enums/room-types";

export interface Classroom {
  name: string;
  types: Array<RoomTypes>;
}
