import {Room} from "../room";

export interface FreeClassrooms {
  freeClassRooms: Array<Room>;
}
