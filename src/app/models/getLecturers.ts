import {Lecturer} from "./lecturer";

export interface GetLecturers {
  lecturers: Array<Lecturer>
}
