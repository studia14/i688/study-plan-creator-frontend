import {RoomTypes} from "./enums/room-types";

export interface Room {
  id: number;
  name: string;
}
