import {RoomTypes} from "./enums/room-types";

export interface GetRooms {
  rooms: Array<Room>
}

export interface Room {
  id: number;
  name: string;
  types: Array<RoomTypes>;
}
