export interface Semester {
  id: number;
  name: string;
  degree: string;
  number: number;
  types: Array<string>
}
