export interface PostLecture {
  lecturer: string;
  classroom: string;
  year: string;
  interval: string;
  weekDay: string;
  time: string;
  name: string;
  deansGroups: Array<string>;
}
