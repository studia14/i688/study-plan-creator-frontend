import {Lecturer} from "./lecturer";
import {Group} from "./group";
import {Days} from "./enums/days";
import {Room} from "./room";
import {Interval} from "./enums/interval";

export interface Lecture {
  id: number;
  type: string;
  lecturer: Lecturer;
  dateInterval: Interval;
  dayOfWeek: Days;
  hour: string;
  classroom: Room;
  name: string;
  groups: Array<Group>;
}
