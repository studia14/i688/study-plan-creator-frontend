import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ScheduleTopBarComponent } from './schedule/schedule-top-bar/schedule-top-bar.component';
import { ScheduleListComponent } from './schedule/schedule-list/schedule-list.component';
import { ScheduleTableComponent } from './schedule/schedule-table/schedule-table.component';
import { ScheduleComponent } from './schedule/schedule/schedule.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSelectModule} from "@angular/material/select";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatCheckboxModule} from "@angular/material/checkbox";
import { LoginComponent } from './auth/login/login.component';
import { SingleLectureComponent } from './components/single-lecture/single-lecture.component';
import { LecturePipe } from './shared/pipes/lecture.pipe';
import {MatSidenavModule} from "@angular/material/sidenav";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {MatMenuModule} from "@angular/material/menu";
import {MatDialogModule} from "@angular/material/dialog";
import {AdminModule} from "./admin-panel/admin.module";
import {LoadingDataStatusInterceptor} from "./shared/interceptors/LoadingDataStatusInterceptor";
import {TokenInterceptor} from "./shared/interceptors/TokenInterceptor";

@NgModule({
  declarations: [
    AppComponent,
    ScheduleTopBarComponent,
    ScheduleListComponent,
    ScheduleTableComponent,
    ScheduleComponent,
    PageNotFoundComponent,
    LoginComponent,
    SingleLectureComponent,
    LecturePipe
  ],
  imports: [
    BrowserModule,
    AdminModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSelectModule,
    MatIconModule,
    MatInputModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatMenuModule,
    MatDialogModule,
    BrowserAnimationsModule
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: LoadingDataStatusInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
