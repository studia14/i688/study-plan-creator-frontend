import {Injectable} from '@angular/core';
import {LECTURERS} from "../schedule/mock-lectures";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {User} from "../models/user";
import {Observable} from "rxjs";
import {Lecture} from "../models/lecture";
import {ROOMS} from "./mocks/mock-rooms";
import {GROUPS} from './mocks/mock-groups';
import {environment} from "../../environments/environment.prod";
import {PostLecture} from "../models/postLecture";
import {PostGeneratorInit} from "../models/postGeneratorInit";
import {Lecturer, PostLecturer} from "../models/lecturer";
import {GetRooms, Room} from "../models/getRooms";
import {PutRoom} from "../models/putRoom";
import {UserRes} from "../models/responses/userRes";
import {PostUser} from "../models/postUser";
import {LectureTypesResponse} from "@study/study-plan-creator/lib/lectureTypesResponse";
import {AddNewLectureTypeRequest} from "@study/study-plan-creator/lib/addNewLectureTypeRequest";
import {AddSemesterRequest} from "@study/study-plan-creator/lib/addSemesterRequest";
import {GetSemestersResponse} from "@study/study-plan-creator/lib/getSemestersResponse";
import {map} from "rxjs/operators";
import {Semester} from "@study/study-plan-creator/lib/semester";
import {GetLecturers} from "../models/getLecturers";
import {ScheduleSearchFilter} from '../schedule/ScheduleSearchFilter';
import {GetDeansGroupsResponse} from '@study/study-plan-creator/lib/getDeansGroupsResponse';
import {DeansGroup} from '@study/study-plan-creator/lib/deansGroup';
import {AddNewDeansGroupRequest} from '@study/study-plan-creator/lib/addNewDeansGroupRequest';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  classrooms = ROOMS;
  groups = GROUPS;
  lecturers = LECTURERS;
  API_PATH: string = 'http://localhost:8080/';
  //API_PATH: string = 'http://studyplancreator.kompikownia.pl:9001/';
  private API_URL = environment.API_URL;
  private KEYCLOAK_URL = environment.KEYCLOAK_URL;

  private defaultAuthReq: PostUser = {
    client_id: 'study-plan-creator',
    username: 'admin',
    password: 'admin',
    grant_type: 'password'
  }

  // TODO add authorization headers to requests
  private userToken: string = 'xxxxxx';

  constructor(private http: HttpClient) { }

  // AUTH REQUESTS

  // TODO fetch user from api

  login(username: string, password: string): Observable<UserRes> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    var urlencoded = new URLSearchParams();
    urlencoded.append( 'grant_type','password');
    urlencoded.append('client_id','study-plan-creator-app');
    urlencoded.append('username', username);
    urlencoded.append('password', password);
    return this.http.post<User>(this.KEYCLOAK_URL + '/auth/realms/study-plan-creator/protocol/openid-connect/token', urlencoded.toString(),
      { headers });
  }

  // LECTURES REQUESTS

  getLectures(filter?: ScheduleSearchFilter):Observable<Lecture[]> {
    let params = new HttpParams();
    if(filter && filter.lecturer) {
      params = params.set('lecturer', filter.lecturer);
    }
    if(filter && filter.group) {
      params = params.set('group', filter.group);
    }
    if(filter && filter.room) {
      params = params.set('classroom', filter.room);
    }
    return this.http.get<Lecture[]>(this.API_URL + '/lectures', {
      params: params
    });
  }

  postLectures(lecture: PostLecture):Observable<any> {
    return this.http.post(this.API_URL + '/lectures', lecture);
  }

  // LECTURE TYPES

  getLectureTypes():Observable<LectureTypesResponse[]> {
    return this.http.get<LectureTypesResponse[]>(this.API_URL + '/lecture-types');
  }

  getSemesters(): Observable<Array<Semester>> {
    return this.http.get<GetSemestersResponse>(this.API_URL + '/semester').pipe(
      map(semesterResponse => semesterResponse.semesters));
  }

  postSemester(semester: AddSemesterRequest): Observable<any> {
    return this.http.post<AddSemesterRequest>(this.API_URL + '/semester', semester);
  }

  postLectureTypes(lectureTypeRequest: AddNewLectureTypeRequest):Observable<any> {
    return this.http.post(this.API_URL + '/lecture-types', lectureTypeRequest);
  }

  putLectureTypes(id: number, name: string):Observable<any> {
    return this.http.put(this.API_URL + '/lecture-types/' + id, name);
  }

  deleteLectureTypes(id: number):Observable<any> {
    return this.http.delete(this.API_URL + '/lecture-types/' + id);
  }

  // GENERATOR REQUESTS

  initGenerator(initData: PostGeneratorInit):Observable<any> {
    return this.http.post(this.API_URL + '/generator/init', initData);
  }

  // ROOMS REQUESTS

  getRooms() {
    //return this.classrooms;
    return this.http.get<GetRooms>(this.API_URL + '/room');
  }


  postRoom(room: Room):Observable<any> {
    return this.http.post(this.API_URL + '/room', room);
  }

  putRoom(id: number, room: PutRoom):Observable<any> {
    return this.http.put(this.API_URL + '/room/' + id, room);
  }

  deleteRoom(id: number):Observable<any> {
    return this.http.delete(this.API_URL + '/room/' + id);
  }


  // GROUPS

  getGroups(): Observable<Array<DeansGroup>> {
    return this.http.get<GetDeansGroupsResponse>(this.API_URL + '/deans-groups/')
      .pipe(map(deansGroupResponse => deansGroupResponse.deansGroups));
  }

  postGroup(deansGroup: DeansGroup): Observable<Object> {
    var request : AddNewDeansGroupRequest = {
      deansGroup: deansGroup
    };
    return this.http.post(this.API_URL + '/deans-groups/', request);
  }

  deleteGroup(id: number): Observable<Object> {
    return this.http.delete(this.API_URL + '/deans-groups/' + id);
  }
  // LECTURERS REQUESTS

  getLecturers(): Observable<GetLecturers> {
    return this.http.get<GetLecturers>(this.API_URL + '/lecturers');
  }

  postLecturer(lecturer: Lecturer) {
    return this.http.post(this.API_URL + '/lecturers', lecturer);
  }

  putLecturer(id: number, lecturer: PostLecturer) {
    return this.http.put(this.API_URL + '/lecturers/' + id, lecturer);
  }

  deleteLecturer(id: number) {
    return this.http.delete(this.API_URL + '/lecturers/' + id);
  }

  getLecturerById(id: number):Observable<Lecturer> {
    return this.http.get<Lecturer>(this.API_URL + '/lecturers/' + id);
  }
}
