import {RoomTypes} from "../../models/enums/room-types";
import {Classroom} from "../../models/responses/classroom";

export const ROOMS:Classroom[] = [
  {
    name: '334D',
    types: [ RoomTypes.CWICZENIOWA, RoomTypes.SEMINARYJNA]
  },
  {
    name: '335D',
    types: [ RoomTypes.ZWYKLA, RoomTypes.WYKLADOWA, RoomTypes.CWICZENIOWA]
  },
  {
    name: '336D',
    types: [ RoomTypes.CWICZENIOWA]
  },
  {
    name: '337D',
    types: [ RoomTypes.SEMINARYJNA]
  }
]
