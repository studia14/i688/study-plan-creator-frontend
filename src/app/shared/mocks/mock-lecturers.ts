import { Lecturer } from "src/app/models/lecturer";

export const Lecturers:Lecturer[] = [
  {
    id: 1,
    name: 'Jacek',
    surname: "Placek",
    title: "doktorek",
    canTeach: [{
        classId :1,
        className :'programowanie w c'
    },
    {
      classId :2,
      className :'programowanie w java'
    }
]
    
  },
  {
    id: 1,
    name: 'Jan',
    surname: "Kowalski",
    title: "mgr.",
    canTeach: [{
        classId :2,
        className :'programowanie w c 42'
    },
    {
      classId :3,
      className :'zapychacz'
    },
    {
      classId :4,
      className :'zapychacz2'
    }
]
    
  }
] 