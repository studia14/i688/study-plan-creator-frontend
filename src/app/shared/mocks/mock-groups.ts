import { Group } from "src/app/models/group";

export const GROUPS:Group[] = [
  {
    id: 1,
    name: '31A',
  },
  {
    id: 2,
    name: '21B',
  },{
    id: 3,
    name: '32A',
  },
  {
    id: 4,
    name: '32B',
  },
  {
    id: 5,
    name: '41A',
  },
  {
    id: 6,
    name: '11B',
  },
]
