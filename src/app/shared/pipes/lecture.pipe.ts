import {Pipe, PipeTransform} from '@angular/core';
import {Interval} from "../../models/enums/interval";

@Pipe({
  name: 'lecture'
})
export class LecturePipe implements PipeTransform {

  transform(value: Interval): string {
    switch (value) {
      case Interval.EVERY_WEEK: {
        return 'co tydzień';
      }
      case Interval.EVEN_WEEK: {
        return 'tygodnie parzyste'
      }
      case Interval.ODD_WEEK: {
        return 'tygodnie nieparzyste'
      }
    }
    return 'nie zdefiniowano';
  }

}
