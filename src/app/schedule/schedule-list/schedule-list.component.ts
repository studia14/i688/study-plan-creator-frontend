import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ELEMENTS} from "../mock-elements";
import {Room} from "../../models/room";
import {ApiService} from "../../shared/api.service";
import {Lecturer} from "../../models/lecturer";
import {Group} from "../../models/group";
import {CurrentSchedule} from "../schedule/schedule.component";
import { DeansGroup } from '@study/study-plan-creator/lib/deansGroup';

@Component({
  selector: 'app-schedule-list',
  templateUrl: './schedule-list.component.html',
  styleUrls: ['./schedule-list.component.css']
})
export class ScheduleListComponent implements OnInit {

  categories: Category[] = [
    {value: 'rooms', viewValue: 'Sale'},
    {value: 'teachers', viewValue: 'Prowadzący'},
    {value: 'groups', viewValue: 'Grupy'},
  ];

  elements = ELEMENTS;
  classrooms: Room[] = [];
  lecturers: Lecturer[] = [];
  groups: DeansGroup[] = [];
  allListItems: ListItem[] = [];
  selectedCategory: string = '';

  @Output() selectedItemChanged = new EventEmitter<CurrentSchedule>();

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.getRooms();
    this.getLecturers();
    this.getGroups();
    this.makeListOfAllItems();
  }

  private getRooms() {
    this.apiService.getRooms().subscribe(res => {
      console.log(res);
      this.classrooms = res.rooms;
    });
  }

  private getLecturers() {
    this.apiService.getLecturers().subscribe(res => {
      this.lecturers = res.lecturers;
    });
  }

  private getGroups() {
    this.apiService.getGroups().subscribe(deansGroups => {
      this.groups = deansGroups;
    })
  }

  private makeListOfAllItems() {
    this.classrooms.forEach(classroom => this.allListItems.push({id: classroom.id, name: classroom.name, category: 'rooms'}));
    this.lecturers.forEach(lecturer => this.allListItems.push({id: lecturer.id, name: `${lecturer.name} ${lecturer.surname}` , category: 'teachers'}));
    this.groups.forEach(group => this.allListItems.push({id: group.id, name: group.name, category: 'groups'}));
  }

  refreshList() {
    switch (this.selectedCategory) {
      case 'rooms': {
        this.allListItems = [];
        this.classrooms.forEach(classroom => this.allListItems.push({id: classroom.id, name: classroom.name, category: 'rooms'}));
        break;
      }
      case 'teachers': {
        this.allListItems = [];
        this.lecturers.forEach(lecturer => this.allListItems.push({id: lecturer.id, name: `${lecturer.name} ${lecturer.surname}`, category: 'teachers'}));
        break;
      }
      case 'groups': {
        this.allListItems = [];
        this.groups.forEach(group => this.allListItems.push({id: group.id, name: group.name, category: 'groups'}));
        break;
      }
    }
  }

  refreshSchedule(item: ListItem) {
    this.selectedItemChanged.emit({elementId: item.id, elementCategory: item.category, elementName: item.name});
  }
}

export interface Category {
  value: string;
  viewValue: string
}

export interface ListItem {
  id: number;
  name: string;
  category: string;
}
