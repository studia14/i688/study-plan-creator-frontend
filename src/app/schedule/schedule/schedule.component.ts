import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { ApiService } from '../../shared/api.service';
import { Lecture } from '@study/study-plan-creator/lib/lecture';
import { ScheduleSearchFilter } from '../ScheduleSearchFilter';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit, AfterViewInit {

  lectures: Lecture[] = [];
  showFiller = false;
  @ViewChild('schedule') scheduleDiv: ElementRef | undefined;

  currentSchedule: CurrentSchedule = {elementCategory: '', elementId: -1, elementName: ''};

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.refreshData();
  }

  changeStyle() {
    this.scheduleDiv?.nativeElement.classList.remove('right')
    this.scheduleDiv?.nativeElement.classList.toggle('right-sidenav-on')
  }

  ngAfterViewInit(): void {
  }

  changeStyleClose() {
    this.scheduleDiv?.nativeElement.classList.remove('right-sidenav-on')
    this.scheduleDiv?.nativeElement.classList.toggle('right')
  }

  sendNewItemToView(item: CurrentSchedule) {
    this.currentSchedule = item;
    if(this.currentSchedule.elementCategory === 'teachers') {
      let filter: ScheduleSearchFilter = {
        lecturer: this.currentSchedule.elementName
      }
      this.search(filter);
    }
    if(this.currentSchedule.elementCategory === 'rooms') {
      let filter: ScheduleSearchFilter = {
        room: this.currentSchedule.elementName
      }
      this.search(filter);
    }
    if(this.currentSchedule.elementCategory === 'groups') {
      let filter: ScheduleSearchFilter = {
        group: this.currentSchedule.elementName
      }
      this.search(filter);
    }
  }

  search($event: ScheduleSearchFilter) {
    this.refreshData($event);
  }

  private refreshData(filter?: ScheduleSearchFilter) {
    this.api.getLectures(filter).subscribe(res => {
      // @ts-ignore
      this.lectures = res.lectures;
    });
  }
}

export interface CurrentSchedule {
  elementCategory: string;
  elementId: number;
  elementName: string;
}
