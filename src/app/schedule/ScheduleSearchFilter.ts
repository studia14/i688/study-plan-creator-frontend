export interface ScheduleSearchFilter {
  group?: string;
  lecturer?: string;
  room?: string;
}
