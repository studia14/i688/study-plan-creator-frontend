import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ApiService} from "../../shared/api.service";
import {Days} from "../../models/enums/days";
import {CurrentSchedule} from "../schedule/schedule.component";
import { Lecture } from '@study/study-plan-creator/lib/lecture';

@Component({
  selector: 'app-schedule-table',
  templateUrl: './schedule-table.component.html',
  styleUrls: ['./schedule-table.component.css']
})
export class ScheduleTableComponent implements OnChanges {

  @Input()
  lectures: Lecture[] = [];

  @Input() scheduleToView: CurrentSchedule | undefined;
  mon8: Lecture[] = [];
  mon10: Lecture[] = [];
  mon12: Lecture[] = [];
  mon14: Lecture[] = [];
  tu8: Lecture[] = [];
  tu10: Lecture[] = [];
  tu12: Lecture[] = [];
  tu14: Lecture[] = [];
  we8: Lecture[] = [];
  we10: Lecture[] = [];
  we12: Lecture[] = [];
  we14: Lecture[] = [];
  th8: Lecture[] = [];
  th10: Lecture[] = [];
  th12: Lecture[] = [];
  th14: Lecture[] = [];
  fr8: Lecture[] = [];
  fr10: Lecture[] = [];
  fr12: Lecture[] = [];
  fr14: Lecture[] = [];

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.prepareData();
  }

  private prepareData() {
    this.mon8 = [];
    this.mon10 = [];
    this.mon12 = [];
    this.mon14 = [];
    this.tu8 = [];
    this.tu10 = [];
    this.tu12 = [];
    this.tu14 = [];
    this.we8 = [];
    this.we10 = [];
    this.we12 = [];
    this.we14 = [];
    this.th8 = [];
    this.th10 = [];
    this.th12 = [];
    this.th14 = [];
    this.fr8 = [];
    this.fr10 = [];
    this.fr12 = [];
    this.fr14 = [];
    if(!this.lectures) {
      return;
    }
    // get specific lectures based on selected ID in category
    let lecturesToShow: Lecture[] = [];
/*    if(this.scheduleToView?.elementCategory === 'rooms') {
      lecturesToShow = this.lectures.filter(lecture => lecture.classroom.id === this.scheduleToView?.elementId)
    } else if(this.scheduleToView?.elementCategory === 'teachers') {
      lecturesToShow = this.lectures.filter(lecture => lecture.lecturer.id === this.scheduleToView?.elementId)
    } else if(this.scheduleToView?.elementCategory === 'groups') {*/
    // ...
    this.lectures.forEach(lecture => {
      switch (lecture.hour) {
        case '08:00 - 10:00': {
          switch (lecture.dayOfWeek) {
            case Days.MONDAY: this.mon8.push(lecture);
              break;
            case Days.TUESDAY: this.tu8.push(lecture);
              break;
            case Days.WEDNESDAY: this.we8.push(lecture);
              break;
            case Days.THURSDAY: this.th8.push(lecture);
              break;
            case Days.FRIDAY: this.fr8.push(lecture);
              break;
          }
          break;
        }
        case '10:00 - 12:00': {
          switch (lecture.dayOfWeek) {
            case Days.MONDAY: this.mon10.push(lecture);
              break;
            case Days.TUESDAY: this.tu10.push(lecture);
              break;
            case Days.WEDNESDAY: this.we10.push(lecture);
              break;
            case Days.THURSDAY: this.th10.push(lecture);
              break;
            case Days.FRIDAY: this.fr10.push(lecture);
              break;
          }
          break;
        }
        case '12:00 - 14:00': {
          switch (lecture.dayOfWeek) {
            case Days.MONDAY: this.mon12.push(lecture);
              break;
            case Days.TUESDAY: this.tu12.push(lecture);
              break;
            case Days.WEDNESDAY: this.we12.push(lecture);
              break;
            case Days.THURSDAY: this.th12.push(lecture);
              break;
            case Days.FRIDAY: this.fr12.push(lecture);
              break;
          }
          break;
        }
        case '14:00 - 16:00': {
          switch (lecture.dayOfWeek) {
            case Days.MONDAY: this.mon14.push(lecture);
              break;
            case Days.TUESDAY: this.tu14.push(lecture);
              break;
            case Days.WEDNESDAY: this.we14.push(lecture);
              break;
            case Days.THURSDAY: this.th14.push(lecture);
              break;
            case Days.FRIDAY: this.fr14.push(lecture);
              break;
          }
          break;
        }
      }
    })
  }
}
