import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import { ScheduleSearchFilter } from '../ScheduleSearchFilter';

@Component({
  selector: 'app-schedule-top-bar',
  templateUrl: './schedule-top-bar.component.html',
  styleUrls: ['./schedule-top-bar.component.css']
})
export class ScheduleTopBarComponent {

  @Output()
  onSearch: EventEmitter<ScheduleSearchFilter> = new EventEmitter<ScheduleSearchFilter>();

  value = '';
  groups = false;
  teachers = false;
  rooms = false;

  search() {
    if(this.groups) {
      this.onSearch.emit({
        group: this.value
      })
    }
    if(this.teachers) {
      this.onSearch.emit({
        lecturer: this.value
      });
    }
    if(this.rooms) {
      this.onSearch.emit({
        room: this.value
      });
    }
  }
}
