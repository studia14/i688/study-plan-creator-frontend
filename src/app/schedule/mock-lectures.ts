import {LectureTypes} from "../models/enums/lecture-types";
import {Lecturer} from "../models/lecturer";
import {Lecture} from "../models/lecture";
import {Days} from "../models/enums/days";
import {Interval} from "../models/enums/interval";
import {Room} from "../models/room";
import {Group} from "../models/group";
import {RoomTypes} from "../models/enums/room-types";

export const LECTURERS: Lecturer[] = [
  {
    id: 1,
    name: 'Marian',
    surname: 'Nowak',
    title: "mgr",
    canTeach:[{
      classId: 1,
      className: "zajecia nr1"
    },
  {
    classId: 2,
    className: "zajecia nr2"
  }]
  },{
    id: 2,
    name: 'Jan',
    surname: 'Ulung',
    title: "dr",
    canTeach:[{
      classId: 3,
      className: "zajecia nr3"
    },
  {
    classId: 4,
    className: "zajecia nr4"
  }]
  }
]

export const ROOMS: Room[] = [
  {
    id: 1,
    name: '33D'
  }
];

export const GROUPS: Group[] = [
  {
    id: 1,
    name: '33d'
  }
];

export const LECTURES: Lecture[] = [
  {
    id: 1,
    type: LectureTypes.LABORATORY,
    lecturer: LECTURERS[0],
    dateInterval: Interval.EVERY_WEEK,
    dayOfWeek: Days.MONDAY,
    hour: "10:00",
    classroom: ROOMS[0],
    name: 'Programowanie',
    groups: [GROUPS[0]]
  },
]
