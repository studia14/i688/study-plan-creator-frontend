import { Component, OnInit } from '@angular/core';
import {ApiService} from "../../shared/api.service";
import {MatDialog} from "@angular/material/dialog";
import {DialogComponent} from "../../components/dialog/dialog.component";
import { Lecturer } from 'src/app/models/lecturer';
import {GetLecturers} from "../../models/getLecturers";

@Component({
  selector: 'app-lecturers-list',
  templateUrl: './lecturers-list.component.html',
  styleUrls: ['./lecturers-list.component.css']
})
export class LecturersListComponent implements OnInit {

  lecturers: Lecturer[] = [];
  constructor(private api: ApiService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.api.getLecturers().subscribe(res => {
      console.log(res);
      this.lecturers = res.lecturers;
    });
  }

  openEditDialog(lecturer: Lecturer) {
    this.dialog.open(DialogComponent, {
      data: {
        lecturer: lecturer,
      }
    });
  }
}
