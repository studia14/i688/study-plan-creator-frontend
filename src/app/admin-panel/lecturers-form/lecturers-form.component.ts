import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {Lecture} from "../../models/lecture";
import {ApiService} from "../../shared/api.service";
import {LectureType} from "@study/study-plan-creator/lib/lectureType";

@Component({
  selector: 'app-lecturers-form',
  templateUrl: './lecturers-form.component.html',
  styleUrls: ['./lecturers-form.component.css']
})
export class LecturersFormComponent implements OnInit {
  @Input() operationType: string | undefined;
  constructor(fb: FormBuilder, private api: ApiService) {
    this.addNewLecturer = fb.group({
      id:'',
      name: '',
      surname: '',
      title: '',
      canTeach: Array
    });
  }

  addNewLecturer: FormGroup;

  lectures: LectureType[] = [];

  ngOnInit(): void {
    this.api.getLectureTypes().subscribe(res => {
      // @ts-ignore
      this.lectures = res.lectureTypes;
    });
  }

  postLecturer() {
    console.log(this.addNewLecturer.value);
  }
}
