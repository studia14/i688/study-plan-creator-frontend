import { Component, Input, OnInit } from '@angular/core';
import {ApiService} from "../../shared/api.service";
import {MatDialog} from "@angular/material/dialog";
import {DialogComponent} from "../../components/dialog/dialog.component";
import { Room } from '@study/study-plan-creator/lib/room';

@Component({
  selector: 'app-rooms-list',
  templateUrl: './rooms-list.component.html',
  styleUrls: ['./rooms-list.component.css']
})
export class RoomsListComponent implements OnInit {

  @Input()
  rooms: Room[];
  constructor(private api: ApiService, public dialog: MatDialog) { }

  ngOnInit(): void {
    //this.rooms = this.api.getRooms();
  }

  openEditDialog(room: Room) {
    this.dialog.open(DialogComponent, {
      data: {
        room: room,
      }
    });
  }
}
