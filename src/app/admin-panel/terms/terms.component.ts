import { Component, OnInit } from '@angular/core';
import {Location} from "@angular/common";
import {ApiService} from "../../shared/api.service";
import {Semester} from "@study/study-plan-creator/lib/semester";

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css']
})
export class TermsComponent implements OnInit {

  semesters: Semester[] = [];

  constructor(private location: Location,
              private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.getSemesters().subscribe(semestersArray => {
      this.semesters = semestersArray.map(semester => semester);
    })
  }

  backClicked() {
    this.location.back();
  }

}
