import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AdminPanelComponent} from "./admin-panel/admin-panel.component";
import {RoomsComponent} from "./rooms/rooms.component";
import {RoomsFormComponent} from "./rooms-form/rooms-form.component";
import {RoomsListComponent} from "./rooms-list/rooms-list.component";
import {AdminRoutingModule} from "./admin-routing.module";
import {AppRoutingModule} from "../app-routing.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {MatSelectModule} from "@angular/material/select";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatMenuModule} from "@angular/material/menu";
import {MatDialogModule} from "@angular/material/dialog";
import {NavigationBarComponent} from "../components/navigation-bar/navigation-bar.component";
import {DialogComponent} from "../components/dialog/dialog.component";
import { GroupsComponent } from './groups/groups.component';
import { GroupsFormComponent } from './groups-form/groups-form.component';
import { GroupsListComponent } from './groups-list/groups-list.component';
import { LecturersComponent } from './lecturers/lecturers.component';
import { LecturersFormComponent } from './lecturers-form/lecturers-form.component';
import { LecturersListComponent } from './lecturers-list/lecturers-list.component';
import { TermsComponent } from './terms/terms.component';
import { TermsListComponent } from './terms-list/terms-list.component';
import { TermsFormComponent } from './terms-form/terms-form.component';
import { GeneratorComponent } from './generator/generator.component';
import { MatRadioModule } from '@angular/material/radio';


@NgModule({
  declarations: [
    AdminPanelComponent,
    RoomsComponent,
    RoomsFormComponent,
    RoomsListComponent,
    GroupsComponent,
    GroupsFormComponent,
    GroupsListComponent,
    LecturersComponent,
    LecturersFormComponent,
    LecturersListComponent,
    NavigationBarComponent,
    DialogComponent,
    TermsComponent,
    TermsListComponent,
    TermsFormComponent,
    GeneratorComponent,
  ],
    imports: [
        CommonModule,
        AdminRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        MatSelectModule,
        MatIconModule,
        MatInputModule,
        MatCheckboxModule,
        MatSidenavModule,
        MatMenuModule,
        MatDialogModule,
        MatRadioModule,
    ]
})
export class AdminModule { }
