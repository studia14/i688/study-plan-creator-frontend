import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {RoomTypes} from "../../models/enums/room-types";

@Component({
  selector: 'app-rooms-form',
  templateUrl: './rooms-form.component.html',
  styleUrls: ['./rooms-form.component.css']
})
export class RoomsFormComponent implements OnInit {
  @Input() operationType: string | undefined;
  constructor(fb: FormBuilder) {
    this.addNewRoom = fb.group({
      name: '',
      ex: false,
      lec: false,
      lab: false,
      sem: false,
      normal: false
    });
  }

  roomTypes: [] = []

  addNewRoom: FormGroup;

  ngOnInit(): void {
  }

  postRoom() {
    console.log(this.addNewRoom.value);
  }
}
