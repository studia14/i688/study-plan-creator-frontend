import {Component, Input, OnInit} from '@angular/core';
import {ApiService} from "../../shared/api.service";
import {Semester} from "@study/study-plan-creator/lib/semester";

@Component({
  selector: 'app-terms-list',
  templateUrl: './terms-list.component.html',
  styleUrls: ['./terms-list.component.css']
})
export class TermsListComponent implements OnInit {

  @Input()
  semesters: Semester[] = [];

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
  }

}
