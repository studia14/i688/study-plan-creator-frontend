import { Component, OnInit } from '@angular/core';
import {Location} from "@angular/common";
import { Classroom } from '@study/study-plan-creator/lib/classroom';
import { ApiService } from '../../shared/api.service';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit {

  constructor(private location: Location,
              private api: ApiService) { }

  rooms: Classroom[];

  ngOnInit(): void {
    this.api.getRooms().subscribe(roomsResponse => {
      this.rooms = roomsResponse.rooms;
    })
  }

  backClicked() {
    this.location.back();
  }
}
