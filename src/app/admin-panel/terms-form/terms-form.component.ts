import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {ApiService} from "../../shared/api.service";
import {LectureType} from "@study/study-plan-creator/lib/lectureType";

@Component({
  selector: 'app-terms-form',
  templateUrl: './terms-form.component.html',
  styleUrls: ['./terms-form.component.css']
})
export class TermsFormComponent implements OnInit {
  @Input() operationType: string | undefined;
  constructor(fb: FormBuilder, private api: ApiService) {
    this.addNewTerm = fb.group({
      name: '',
      degree: '',
      number: '',
      lectureTypes: new FormArray([]),
    });
  }

  get lectureTypesFormArray() {
    return this.addNewTerm.controls.lectureTypes as FormArray;
  }

  addNewTerm: FormGroup;
  lectures: LectureType[] = [];

  ngOnInit(): void {
    this.api.getLectureTypes().subscribe(res => {
      // @ts-ignore
      this.lectures = res.lectureTypes;
      this.lectures.forEach(lecture => {
        this.lectureTypesFormArray.push(new FormControl(false));
      })
      console.log(this.lectureTypesFormArray);
    });
  }

  postTerm() {
    const activatedLectureTypes: LectureType[] = [];
    this.addNewTerm.controls.lectureTypes.value
      .forEach((element: any, index: any) => {
        if(element) {
          activatedLectureTypes.push(this.lectures[index])
        }
      })
    const request = {
      name: this.addNewTerm.controls.name.value,
      degree: this.addNewTerm.controls.degree.value,
      number: this.addNewTerm.controls.number.value,
      types: activatedLectureTypes
    }
    this.api.postSemester(request).subscribe();
  }
}
