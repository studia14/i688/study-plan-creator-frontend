import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder } from "@angular/forms";
import { ApiService } from "../../shared/api.service";
import { LectureType } from "@study/study-plan-creator/lib/lectureType";
import { DeansGroup } from '@study/study-plan-creator/lib/deansGroup';
import { Semester } from '@study/study-plan-creator/lib/semester';

@Component({
  selector: 'app-groups-form',
  templateUrl: './groups-form.component.html',
  styleUrls: ['./groups-form.component.css']
})
export class GroupsFormComponent implements OnInit {
  @Input() operationType: string | undefined;
  @Input() group: DeansGroup;
  @Input() semesters: Semester[];
  @Output()
  submitClicked: EventEmitter<DeansGroup> = new EventEmitter<DeansGroup>();
  constructor(private api: ApiService) {
  }


  lectures: LectureType[] = [];

  ngOnInit(): void {
  }
}
