import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AdminPanelComponent} from "./admin-panel/admin-panel.component";
import {AuthGuard} from "../shared/guards/auth.guard";
import {RoomsComponent} from "./rooms/rooms.component";
import { GroupsComponent } from './groups/groups.component';
import { LecturersComponent } from './lecturers/lecturers.component';
import {TermsComponent} from "./terms/terms.component";
import {GeneratorComponent} from "./generator/generator.component";

const routes: Routes = [
  {
    path: 'rooms',
    component: RoomsComponent,
  },
  {
    path: 'panel',
    component: AdminPanelComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'groups',
    component:GroupsComponent
  },
  {
    path: 'lecturers',
    component:LecturersComponent
  },
  {
    path: 'terms',
    component: TermsComponent
  },
  {
    path: 'generate',
    component: GeneratorComponent
  }
  // {
  //   path: 'panel',
  //   component: AdminPanelComponent,
  //   canActivate: [AuthGuard],
  //   children: [
  //     {
  //       path: '',
  //       canActivateChild: [AuthGuard],
  //       children: [
  //         {
  //           path: 'rooms',
  //           component: RoomsComponent
  //         }
  //       ]
  //     }
  //   ]
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
