import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ApiService } from "../../shared/api.service";
import { MatDialog } from "@angular/material/dialog";
import { DeansGroup } from '@study/study-plan-creator/lib/deansGroup';

@Component({
  selector: 'app-groups-list',
  templateUrl: './groups-list.component.html',
  styleUrls: ['./groups-list.component.css']
})
export class GroupsListComponent implements OnInit {

  @Output()
  editClicked: EventEmitter<DeansGroup> = new EventEmitter<DeansGroup>();

  @Output()
  removeClicked: EventEmitter<DeansGroup> = new EventEmitter<DeansGroup>();

  @Input()
  groups: DeansGroup[] = [];
  constructor(private api: ApiService, public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  edit(group: DeansGroup) {
    console.log(group);
    this.editClicked.emit(group);
  }

  remove(group: DeansGroup) {
    this.removeClicked.emit(group);
  }
}
