import { Component, OnInit } from '@angular/core';
import {Location} from "@angular/common";
import { DeansGroup } from '@study/study-plan-creator/lib/deansGroup';
import { ApiService } from '../../shared/api.service';
import { Semester } from '@study/study-plan-creator/lib/semester';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {
  group: DeansGroup = {
    id: -1,
    name: '',
    semesterId: '',
    semesterName: ''
  }
  semesters: Semester[];
  operationType: string;
  groups: DeansGroup[];

  constructor(private location: Location, private api: ApiService) { }


  ngOnInit(): void {
    this.api.getSemesters().subscribe(semesters => {
      this.semesters = semesters;
    })
    this.api.getGroups().subscribe(deansGroups => {
      this.groups = deansGroups;
    });
  }

  backClicked() {
    this.location.back();
  }

  editGroup($event: DeansGroup) {
    this.operationType = 'edit';
    this.group = $event;
  }

  submit(event: DeansGroup) {
    if(this.operationType !== 'edit') {
      this.api.postGroup(event).subscribe(() => {
        this.ngOnInit();
      });
    }
  }

  removeGroup($event: DeansGroup) {
    this.api.deleteGroup($event.id).subscribe(() => {
      this.ngOnInit();
    })
  }
}
