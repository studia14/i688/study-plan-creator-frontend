export const environment = {
  production: true,
  API_URL: 'http://studyplancreator.kompikownia.pl:9001',
  KEYCLOAK_URL: 'http://studyplancreator.kompikownia.pl:9003'
};
